// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "ObjectMacros.h"
#include "ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SOLIDSNAKE_SolidSnakeGameModeBase_generated_h
#error "SolidSnakeGameModeBase.generated.h already included, missing '#pragma once' in SolidSnakeGameModeBase.h"
#endif
#define SOLIDSNAKE_SolidSnakeGameModeBase_generated_h

#define SolidSnake_Source_SolidSnake_SolidSnakeGameModeBase_h_15_RPC_WRAPPERS
#define SolidSnake_Source_SolidSnake_SolidSnakeGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS
#define SolidSnake_Source_SolidSnake_SolidSnakeGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesASolidSnakeGameModeBase(); \
	friend SOLIDSNAKE_API class UClass* Z_Construct_UClass_ASolidSnakeGameModeBase(); \
public: \
	DECLARE_CLASS(ASolidSnakeGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/SolidSnake"), NO_API) \
	DECLARE_SERIALIZER(ASolidSnakeGameModeBase) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SolidSnake_Source_SolidSnake_SolidSnakeGameModeBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesASolidSnakeGameModeBase(); \
	friend SOLIDSNAKE_API class UClass* Z_Construct_UClass_ASolidSnakeGameModeBase(); \
public: \
	DECLARE_CLASS(ASolidSnakeGameModeBase, AGameModeBase, COMPILED_IN_FLAGS(0 | CLASS_Transient), 0, TEXT("/Script/SolidSnake"), NO_API) \
	DECLARE_SERIALIZER(ASolidSnakeGameModeBase) \
	enum {IsIntrinsic=COMPILED_IN_INTRINSIC};


#define SolidSnake_Source_SolidSnake_SolidSnakeGameModeBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASolidSnakeGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASolidSnakeGameModeBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASolidSnakeGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASolidSnakeGameModeBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASolidSnakeGameModeBase(ASolidSnakeGameModeBase&&); \
	NO_API ASolidSnakeGameModeBase(const ASolidSnakeGameModeBase&); \
public:


#define SolidSnake_Source_SolidSnake_SolidSnakeGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API ASolidSnakeGameModeBase(const FObjectInitializer& ObjectInitializer = FObjectInitializer::Get()) : Super(ObjectInitializer) { }; \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API ASolidSnakeGameModeBase(ASolidSnakeGameModeBase&&); \
	NO_API ASolidSnakeGameModeBase(const ASolidSnakeGameModeBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, ASolidSnakeGameModeBase); \
DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(ASolidSnakeGameModeBase); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(ASolidSnakeGameModeBase)


#define SolidSnake_Source_SolidSnake_SolidSnakeGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET
#define SolidSnake_Source_SolidSnake_SolidSnakeGameModeBase_h_12_PROLOG
#define SolidSnake_Source_SolidSnake_SolidSnakeGameModeBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SolidSnake_Source_SolidSnake_SolidSnakeGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	SolidSnake_Source_SolidSnake_SolidSnakeGameModeBase_h_15_RPC_WRAPPERS \
	SolidSnake_Source_SolidSnake_SolidSnakeGameModeBase_h_15_INCLASS \
	SolidSnake_Source_SolidSnake_SolidSnakeGameModeBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define SolidSnake_Source_SolidSnake_SolidSnakeGameModeBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	SolidSnake_Source_SolidSnake_SolidSnakeGameModeBase_h_15_PRIVATE_PROPERTY_OFFSET \
	SolidSnake_Source_SolidSnake_SolidSnakeGameModeBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	SolidSnake_Source_SolidSnake_SolidSnakeGameModeBase_h_15_INCLASS_NO_PURE_DECLS \
	SolidSnake_Source_SolidSnake_SolidSnakeGameModeBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID SolidSnake_Source_SolidSnake_SolidSnakeGameModeBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
